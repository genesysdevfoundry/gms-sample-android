## About Genesys Mobile Services Android Sample

The Genesys Mobile Services (GMS) Android sample illustrates how to implement an Android app that communicates with GMS and implements supported contact scenarios. It is primarily meant to be used by developers as a reference to build an Android app using the GMS Android SDK. This sample app can also be used to test an existing GMS deployment because the scenario parameters are configurable in the GMS Service Management UI.


In this repository, you can find the new Android Sample for GMS 8.5.2, using the GMS Android SDK library. To develop an application using GMS Android SDK, you need:

- Android 4.0 (Ice Cream Sandwich) or higher
- Android studio version 1.4 or higher
- Latest GMS Android SDK library available in  [maven](https://mvnrepository.com/artifact/com.genesys.gms.android/sdk)

> __Important__
>
>  The Android SDK used in this sample includes the Javadoc JAR file that you can import in Android Studio. Once imported, the JAR file also provides automatic completion and documentation.
>

## Prerequisites

To use the Android sample app, install, and start GMS version 8.5.201 and higher. Then, configure your services as follow:

- Create your `gmstester-callback` service in the [Configured Services](https://docs.genesys.com/Documentation/GMS/8.5.2/Help/ConfiguredServices) panel of the Service Management UI.
    - Click create and select `callback` in the __Service Template__ list.
    - Enter `gmstester-callback` for the __Service Name__ and select `sample` in __Common Default Configuration__.
- Deploy and configure the services that you want to use in the __Configured Services__ panel of the Service Management UI or in the GMS Configuration for the Digital Channel APIs. For example, create the `chat.customer-support` service for __CHAT-V2__ in your GMS configuration.
    - See [Adding a Callback Service](https://docs.genesys.com/Documentation/CLBCK/latest/UG/AddingaService) for further details about callback services.
    - See [Configuring the Digital Channels API](https://docs.genesys.com/Documentation/GMS/latest/Deployment/ConfiguringDigitalChannels) for details about Chat V2 configuration.
- Configure [Firebase Cloud Messaging](https://docs.genesys.com/Documentation/GMS/8.5.2/API/PushNotificationService#fcm) in your GMS Configuration. Edit the [fcm](https://docs.genesys.com/Documentation/GMS/8.5.2/Deployment/ConfigurationOptions#Firebase_Cloud_Messaging) section of the GMS configuration to provide your FCM API key.
- Enable push notifications for Firebase Cloud Messaging in your GMS configuration:
    - [pushEnabled=fcm](https://docs.genesys.com/Documentation/Options/Current/GMS/GMS-push#pushEnabled)

> __Important__
>
>  Restart GMS after configuring Firebase Cloud Messaging in the `fcm` and `push` sections.
>

## Features and Scenarios 
The Android Sample supports the __Click to Call__ and __Click to Chat__ scenarios, in addition to Callback Scenarios.

![Connect](img/gms-android-sample-Home_Screen.png)


### Scenarios

![Scenarios](img/gms-android-sample-VOICE-NOW-USERORIG-1.jpg)



|Scenario | Description|
|:--------|:-------------------|
|VOICE-NOW-USERORIG (Voice Callback) | The customer wants to contact the Contact Center immediately. The Callback service provides an access number and an access code (optional) that the customer can dial. Then, the customer's inbound call is processed and routed to an agent.|
|VOICE-WAIT-USERORIG  (Voice Callback) | The customer wants to contact the Contact Center and agrees to wait for an agent. The Callback service notifies the mobile when an agent is ready, then provides an access number and an access code (optional) that the customer can dial. Then, the customer's inbound call is processed and routed to an agent.|
|VOICE-NOW-USERTERM  (Voice Callback) |The customer requests an immediate callback, that is, as soon as the agent is available, the Contact Center dials a call. |
|VOICE-WAIT-USERTERM  (Voice Callback) |The customer requests a callback and agrees to wait for an agent. The Callback service notifies the mobile when an agent is ready and asks if the customer wants to be called back now or later.|
|VOICE-SCHEDULED-USERTERM  (Voice Callback) |The customer requests a callback at a specific date and time based on the Contact Center's Office hours.|
|Chat V2| The customer starts a chat session using Chat V2 from Digital Channel. this |
|CUSTOM| Enables you to test a simple service (not only the Callback service). For instance, you can test a stat service. The sample displays the result in a dialog box. |
|SIMPLE-SERVICE | Enables you to test any of your callback services. |


These scenarios are server-driven, which means that the server instructs the client with the actions needed to carry out the scenario. The client just needs to perform those actions and follow up the dialog with the server. Therefore, the client is flexible enough to support any scenario that is built using the same kind of actions. The following actions are supported:

- `DialNumber` - The app makes a phone call.
- `ConfirmationDialog` - The app displays a message.
- `DisplayMenu` - The app displays a menu for the user to select an item that may affect how the scenario proceeds.
- `StartChat` - The app starts a chat conversation. Asynchronous HTTP notifications (comet messages) are used for receiving chat server events.

> __Note__
> Scenarios are detailed in the __Running the sample__ section below.

### Features 

- Push notifications through Firebase Cloud Messaging are supported. Delayed scenarios are supported by using push notifications only; the app will not poll the server to be notified about agent availability.
- Request custom service
- Server Response logs
- Queue Statistics


## Build the Sample

This sample uses the Gradle build system.

1. First, download the samples by cloning this repository or downloading an archived snapshot.

2. In Android Studio, use the "Import non-Android Studio project" or "Import Project" option. Next, select the root directory that you downloaded.

3. If you get prompted for a Gradle configuration, accept the default settings. Alternatively, use the "gradlew build" command to build the project directly.

Here is the step-by-step user guide to open the Android sample in Android Studio.

### Select "Open an existing Android Studio project"

![Open](img/android_sample_open.png)

### Select the folder where the source was downloaded.

![Select](img/android_sample_select_project.png)

### Android Studio loads, synchronizes, and builds the project.

![Loading](img/android_sample_loading.png)

### The sample opens.

![Build success](img/android_sample_build_success.png)

### Clean the Android project and rebuild.

After cleaning, Android Studio builds the project automatically. If not, navigate to "Build > Rebuild Project" to force the rebuild.

![Clean](img/android_sample_build_clean.png)

### If the build is successful, click "Run" to start the sample.

![Run](img/android_sample_deployment_run.png)

### The "Select deployment target" dialog opens and lists the available targets.

If an Android device is connected, it should be displayed in this list. Select the target or emulator where the sample should launch. As a result, the app installs in the chosen device.

![Target](img/android_sample_deployment_target.png)

### Once the app is installed in the Device or Emulator, the app is launched.
You can also check the successful launch of the app in the terminal of Android Studio. 

![Apk](img/android_sample_apk.png)


## Adding Firebase to the Android App/Project

This sample is delivered with a default google-services.json file. Before you run the sample, you must create your own file.

### Prerequisites

- A device running Android 4.0 (Ice Cream Sandwich) or newer
- Google Play services 15.0.0 or higher

### Adding Firebase to the Project

1. Create a project in the [Firebase Console](https://console.firebase.google.com/)
2. Click "Add Firebase to your Android app" and follow the setup steps.
3. When prompted, enter your app's package name (for example, com.genesys.xxx.xxxx.xxxxx). Ensure to enter the exact package name that your app is using; you can set this parameter only when you add an app to your Firebase project.
4. At the end of this process, download the provided google-services.json file. Note that you can download this file again at any time.
5. Copy the 'google-services.json' file into your project's module folder; typically, this file should be placed in the 'app/' folder. 

> __Note__
> If you have multiple build variants with different package names, ensure that you add each app to your project in the Firebase console.
>

## Adding the GMS Android SDK

Edit the Gradle file (usually located in the 'app/build.gradle' folder) and add the GMS SDK version to the dependencies.

```
app/build.gradle
     
    dependencies {
        ....
        // Genesys GMS SDK module        
        implementation 'com.genesys.gms.android:sdk:<version>'

        ....
        // Important: The below mentioned plugin versions should be the same
        implementation 'com.google.android.gms:play-services-gcm:11.0.1'
        implementation 'com.google.firebase:firebase-messaging:11.0.1'
        ....
    }
    //This line is required at the end of file to avoid dependency collisions
    apply plugin: 'com.google.gms.google-services
```

You can add any additional application plugin dependency to the 'dependencies' section.
the Firebase Version dependencies can be decided based on the Android application requirements.
For more information about the Firebase version, visit https://firebase.google.com/support/release-notes/android.

## Setup Android FCM Client

1. Create a service which extends `FirebaseMessagingService` to perform any message handling beyond receiving notifications on apps in the background. This service receives data payload and notifications in foregrounded apps, sends upstream messages, and so on.

```
<service
    android:name=".FcmMessagingService">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"></action>
    </intent-filter>
</service>

```

2. Create a service that extends `FirebaseInstanceIdService` to handle the creation, rotation, and update of registration tokens. This service is required for sending events to specific devices or creating device groups.

```
<service
    android:name=".FcmInstanceIdService">
    <intent-filter>
        <action android:name="com.google.firebase.INSTANCE_ID_EVENT"/>
    </intent-filter>
</service>

```

## Running the Sample

To run the Android sample, download the APK file and deploy it on your Android device. Alternatively, you can also compile the source code to make your own APK file by using the Android SDK. Using the sample is easy, as long as GMS is already deployed.

![Start up screen](img/gms-android-sample-start.png)

The startup screen is a configuration screen. To enable push notifications to work, remember to:

- Register a project in Google Cloud.
- Enable the Firebase Cloud Messaging (FCM) for Android API. 
- In the app, fill the FCM sender id with your Google Cloud project number. 

For more details, check the previous sections of this page or refer to [Firebase Cloud Messaging - Getting Started](https://firebase.google.com/docs/android/setup#add_firebase_to_your_app).

Click or tap `Scenario` to display the list of available scenarios, then select one of them, for example, `CHAT-V2`.

### Chat-V2 

Click or tap `Scenario` to display the list of available scenarios, then select `CHAT-V2`.

![Start Chat](img/gms-android-sample-CHAT-0.png)

Fill the configuration options with the appropriate values of your GMS environment. 

- Enter your sample service name, for instance, `gmstester-callback`, and the service name used to implement your CHAT-V2 scenario, for example, `customer-service`.
- Enter your API Key.

![Settings](img/gms-android-sample-Settings_Tab.png)

Scroll down and ensure that push notifications are enabled. Then, connect the session by clicking the arrow icon in the top-right corner.

![Enable Notifications](img/gms-android-sample-Enable_Push_Notification.png)

The Chat session starts. You can now send and receive both messages and files. In addition, you are notified when the other participant is typing and you can leave the chat session by clicking the 'X' icon.

![Chat session started](img/gms-android-sample-CHAT-5.png)


### VOICE-NOW-USERORIG

Click or tap `Scenario` to display the list of available scenarios, then select `VOICE-NOW-USERORIG`.   The Callback service provides an access number and an access code (optional) that you can dial. 

![Start Voice-Now ](img/gms-android-sample-VOICE-NOW-USERORIG-a.png)

### VOICE-NOW-USERTERM

Click or tap `Scenario` to display the list of available scenarios, then select `VOICE-NOW-USERTERM`. Then, connect. You will receive a call as soon as an agent is available.

![Select](img/gms-android-sample-VOICE-NOW-USERTERM.png)

### VOICE-WAIT-USERORIG

Click or tap `Scenario` to display the list of available scenarios, then select `VOICE-WAIT-USERORIG`. Then, connect. The Callback service notifies the mobile when an agent is ready, then provides an access number and an access code (optional) that you can dial. 

![Select](img/gms-android-sample-VOICE-WAIT-USERORIG-a.png) 

![Connect](img/gms-android-sample-VOICE-WAIT-USERORIG-b.png) 

### VOICE-SCHEDULED-USERTERM

![Select](img/gms-android-sample-VOICE-SCHEDULED-USERTERM.png) &nbsp; &nbsp; &nbsp;

### Log Section

To review in detail the  communication between the client and the server, use the Log screen that you can open using the __Show log__ option in the startup screen. You can also inspect the standard Android logging, by using the Android logcat tool, and obtain more details or logs on your computer screen. 

> __TIP__
> Filter with the `GenesysService` tag in order to see the client-server communication.
>

## About the Code

The app is designed to be minimal to ensure that you can focus on the implementation of Mobile Services scenarios. It consists of:

- `MainActivity` – Main activity which hosts the tab fragments and handles callback.
- `ConnectFragment` - Displays the app configuration options and allows launching the Mobile Services scenarios.
    - `SettingsFragment` - Displays the service configuration options and interprets the server-driven actions.
    - `QueueFragment` – Holds the queue position and details in case the user is put on hold.
    - `AboutDialogFragment` – Displays the SDK version details.
- ChatActivity – Implements all the key operations related to chat screens. The chat messages are handled by the following classes:
    -  `MessageHolder` – Holds the content and the details of the message sent by both the user and the executive.
    - `MessageListAdapter` – Contains the list of messages sent during the chat session. This class handles and orders the messages which you can view entirely using the scroll option.
- LogActivity - Displays logs.
- Firebase Messaging Service:
    - `FcmMessagingService`  - Performs any message handling beyond receiving notifications on apps in the background. To receive notifications in foregrounded apps, to receive data payload, to send upstream messages.
    - `FcmInstanceIdService` – Handles the creation, rotation, and update of registration tokens. This service is required for sending to specific devices or for creating device groups.
    - For more information regarding the latest FCM library and updates, refer the [Android Documentation](https://firebase.google.com/docs/cloud-messaging/android/client)
- The `ChatClient` class and some companion classes implement chat operations. These classes are meant to be reusable in your code. See the disclaimer below.

### GMS SDK

## Getting Started

Use your favorite editor to edit the ```app/build.gradle``` file.

1. Add the GMS SDK with the correct version to the dependencies sections:

```
repositories {
        jcenter()
}
dependencies {
	...
	    implementation 'com.genesys.gms.android:sdk:<version_from_maven_repo>'
	...
}
```
For example, if you download your SDK from the revision [241](https://mvnrepository.com/artifact/com.genesys.gms.android/sdk/8.5.201.00.rev.241) use `8.5.201.00.rev.241` for the version.

2. Ensure that the Android `minSdkVersion` option is not less than 19 and that the Android `compileSdkVersion` option matches the latest version available (here, `27`).
```
android {
    compileSdkVersion 27
	defaultConfig {
		...
        	minSdkVersion 19
		...
	}
}
```

### Structure

The Android SDK is divided into two parts:

* `Settings`: Handles all the settings that the API needs to connect with GMS and Callback.
* `Chat`: Handles all the actions required during a Chat session.

See the sections below for further details on using these APIs.

> __Important__
>
> The Javadoc is included in your JAR file.
>

## Using Settings

First of all, to use the Settings part of the SDK, you need to implement `SettingsHandler` class and create a `SettingsManager` instance. This instance contains and handles all the settings that you need for each subsequent actions.


For example:

```
public class MainActivity extends AppCompatActivity implements SettingsHandler{

	[...]
	private SettingsManager settingsMgr;

	 @Override
    protected void onCreate(Bundle savedInstanceState) {
	
	    settingsMgr = SettingsManager.createInstance(this, this,
                MySSLSocketFactory.getTrustManager(), MySSLSocketFactory.getSSLSocketFactory());
	}

	@Override
    public void onError(Exception exception) {
		
    }
	
	@Override
    public void dial(DialogResponse dialog) {
	
	}
	
	@Override	
	void availableTimeslots(Map<String, String> var1){
	
	}

}
```

To retrieve the `SettingsManager` instance after the initialization, use the `getInstance` method.
```
SettingsManager settingsMgr = SettingsManager.getInstance();
```

In this handler, you can implement the `dial` method to process the dialog response. The example below provides an example of implementation:

```
    @Override
    public void dial(DialogResponse dialog) {

            [...]
            
            switch (dialog.getAction()) {
                case DIAL:
                    Timber.d("dial func: " + dialog.getLabel());
                    callbackApi.makeCall(Uri.parse(dialog.getTelUrl()));
                    break;
                case MENU:
                    if (dialog.getContent() == null || dialog.getContent().size() == 0) {
                        // It's empty! No menu to show...
                        break;
                    }
                    if(dialog.getId() != null)
                        saveServiceId(dialog.getId());

                    DialogResponse.DialogGroup group = dialog.getContent().get(0);
                    String groupName = group.getGroupName();
                    final List<DialogResponse.GroupContent> groupContents = group.getGroupContent();
                   [...]
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(dialog.getLabel() + "\n" + groupName)
                            .setItems(menuItems, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String url = groupContents.get(which).getUserActionUrl();
                                    switch(which) {
                                        case 0:
                                        case 1:
                                            settingsMgr.startDialog(url);
                                            break;
                                        case 2:
                                            settingsMgr.cancelCallbackQuery(url);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            });
                    builder.create().show();
                    break;
                case CONFIRM:
                    displayToastMsg(dialog.getText());
                    Timber.d("Confirmation message: " + dialog.getText());
                    break;
                default:
                    break;
            }
        }
    }
```

Ensure to save all the settings by using the `saveDatas` method:

```
settingsManager.saveDatas(settings);
```

The `settings` object stores all the useful settings, like the name and telephone number of the user and all server settings such as the address, the port, and so on. The `saveDatas` methods allows to save all the settings and get them later or after closing the application. For example, after an `onResume` event in an activity, retrieve the saved data by calling `SettingsManager.getDatas()`:

```
settings = SettingsManager.getDatas();
```

When you start a call or a chat session as detailed in other sections, ensure to configure all the settings and then use the `connect` method like in this snippet:

```
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
            case R.id.connect:
                Timber.d("Option selected: Connect");
                Map<String, String> params = new HashMap<>();
                saveSettingsData();
                Settings settings = settingsMgr.getDatas();
                String[] scenariosList = getResources().getStringArray(R.array.scenarios);
                String scenario = scenariosList[0];
                int index = getScenarioIndex();

                if(index != -1) {
                    scenario = scenariosList[index];
                }
                params = callbackApi.getScenarioParams(settings, ScenarioType.fromString(scenario));
                if (ScenarioType.fromString(scenario) == ScenarioType.CHAT_V2) {
                    Intent intent = new Intent(this, ChatActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Connecting...", Toast.LENGTH_SHORT).show();
                    settingsMgr.connect(settings.getServiceName(), params);
                }
                break;
            [...]
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
```

> __Important__
>
> Remember that if you want to start a scheduled call, you must set the `dateTime` attribute in the `settings` instance as an ISO 8601 date.
>

## ChatHandler

To start a chat session, create the UI and open your own activity. To create your `ChatActivity` class, extend the `AppCompatActivity` class and implement the `ChatHandler` interface.

While creating `ChatClient`, there is an option to add ssl context as third paramter and the httpClient will be based on the ssl context created.
```
public class ChatActivity extends AppCompatActivity implements ChatHandler {
	[...]
	private ChatClient chatClient;

	@Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        [...]
		//ChatClient(Context, ChatHandler)
		chatClient = new ChatClient(context, this);
        //ChatClient(Context, ChatHandler, SslContextFactory)
        //chatClient = new ChatClient(context, this, MySSLSocketFactory.getSslContextFactory());
        [...]
	}  
    
	@Override
    public void onMessage(ChatV2Response response){
		[...]
	}

	//The url of the downloaded file
	@Override
    public void onFileDownloaded(String url){
		[...]
	}
	
	
	@Override
    public void onFileUploaded(Uri uri){
		[...]
	}
	
	
	@Override
    public void onError(Exception e){
		[...]
	}
    
    [...]

}
```
First create an instance of `ChatClient`, like in the `aMethod` example above. After initializing the API, you can start a Chat session by calling the `connect` method. 

```
chatClient.connect();
```

The `connect` method  automatically starts the chat session and call the "onMessage" or "onError" when it is needed. You can send chat start and stop typing events by calling `startTyping` and `stopTyping` methods, as shown below:

```
chatClient.startTyping()
chatClient.stopTyping()
```

To send a message, simply use the `sendMessage` method.

```
chatClient.sendMessage(message)
```

You can also download files through the `downloadFile` method.

```
chatClient.downloadFile(messageId)
```

To download a file from the chat session, use the messageID that is provided in the response of the "onMessage" event.  The `downloadFile` method returns the URI of the given file where the user can start the download.

To end the chat session, call `disconnect`:

```
chatClient.disconnect()
```

## Adding SSL

SSL settings is defined in MySSLSocketFactory, which creates the ssl context.
```
public class MySSLSocketFactory  {

    private static MySSLSocketFactory mySSLSocketFactory = null;
    private static X509TrustManager trustManager = null;
    private static SSLSocketFactory sslSocketFactory = null;
    private static SslContextFactory contextFactory = null;

    public MySSLSocketFactory() {
        super();
        createTrustManager();
        createSSLSocketFactory();
        createSslContextFactory();
    }

    public static MySSLSocketFactory getInstance() {
        if(mySSLSocketFactory == null) {
            mySSLSocketFactory = new MySSLSocketFactory();
        }
        return mySSLSocketFactory;
    }

    // For retrofit
    private void createTrustManager() {
        try {
            if(trustManager == null) {
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                [...]
    }

    // For retrofit
    private void createSSLSocketFactory() {
        try {
            if(sslSocketFactory == null) {
                SSLContext sslContext = SSLContext.getInstance("TLS");
               [...]
    }

    //For chatClient
    private void createSslContextFactory() {
        final SslContextFactory contextFactory = new SslContextFactory(true);// or  SslContextFactory();
        //Include necessary SSL Settings
        //contextFactory.setIncludeProtocols("TLSv1.2");
      [... ]
    }

    public static SSLSocketFactory getSSLSocketFactory() {
        return sslSocketFactory;
    }

    public static X509TrustManager getTrustManager() {
        return trustManager;
    }

    public static SslContextFactory getSslContextFactory() {
        return contextFactory;
    }
}
```

## Firebase Cloud Messaging
Refer to the instructions detailed in the Android Sample's [Readme](https://bitbucket.org/genesysdevfoundry/gms-sample-android/src/master/README.md) to implement FCM. Once all the firebase instructions are completed (Connect app, add FCM, access the registration token, handle message), you can receive  messages in the `onMessageReceived()` method. Process the message in your handler, for example, as follows:

```
@Override
public void onMessageReceived(RemoteMessage remoteMessage) {
	// Check if message contains a data payload.
    if (remoteMessage.getData().size() > 0) {        
        SettingsManager settingsManager = SettingsManager.getInstance();
        settingsManager.handleMessage(remoteMessage.getData().get("message"));
    }
}
```

## Disclaimer

THIS CODE IS PROVIDED BY GENESYS TELECOMMUNICATIONS LABORATORIES, INC. ("GENESYS") "AS IS" WITHOUT ANY WARRANTY OF ANY KIND. GENESYS HEREBY DISCLAIMS ALL EXPRESS, IMPLIED, OR STATUTORY CONDITIONS, REPRESENTATIONS AND WARRANTIES WITH RESPECT TO THIS CODE (OR ANY PART THEREOF), INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. GENESYS AND ITS SUPPLIERS SHALL NOT BE LIABLE FOR ANY DAMAGE SUFFERED AS A RESULT OF USING THIS CODE. IN NO EVENT SHALL GENESYS AND ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, ECONOMIC, INCIDENTAL, OR SPECIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, ANY LOST REVENUES OR PROFITS).


## License

```
Copyright 2018 Genesys

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

