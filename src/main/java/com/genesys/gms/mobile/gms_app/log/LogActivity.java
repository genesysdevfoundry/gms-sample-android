package com.genesys.gms.mobile.gms_app.log;

import android.app.Activity;
import android.os.Bundle;

import com.genesys.gms.mobile.gms_app.R;

/** Shell container activity for LogFragment */
public class LogActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.log_parent_layout);
  }

}
