package com.genesys.gms.mobile.gms_app;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.genesys.gms.mobile.utils.SettingsManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import android.widget.EditText;

import timber.log.Timber;

/**
 * Created by pbalakri on 06-12-2017.
 */

public class QueueFragment extends Fragment {

    private Button cancelBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_queue, container, false);
        initCancelCallback(rootView);
        return rootView;
    }

    public static QueueFragment create(int position) {
        QueueFragment fragment = new QueueFragment();
        Bundle args = new Bundle();
        args.putInt("preferences", position);
        fragment.setArguments(args);
        return fragment;
    }

    private void initCancelCallback(View view) {
        Timber.d("Initialize Cancel Callback");
        cancelBtn = view.findViewById(R.id.cancelCallbackbutton);
        cancelBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SettingsManager settingsMgr = SettingsManager.getInstance();
                settingsMgr.cancelCallbackQuery(
                        settingsMgr.getDatas().getServiceName(),
                        getServiceId());
            }
        });

    }

    private String getServiceId() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return mSettings.getString("service_id", "");
    }

    private Map<String, String> getrescheduleParams() {
        Map<String, String> params = new HashMap();
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, 5);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String desiredTimeISO = df.format(now.getTimeInMillis())+"Z";
        params.put("_new_desired_time",  desiredTimeISO);
        return params;
    }
}
