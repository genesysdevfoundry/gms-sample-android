package com.genesys.gms.mobile.gms_app.adapter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genesys.gms.mobile.data.api.pojo.ChatV2;
import com.genesys.gms.mobile.gms_app.R;
import com.genesys.gms.mobile.gms_app.message.ChatMessage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by pbalakri on 18-01-2018.
 */

public class MessageListAdapter extends RecyclerView.Adapter<MessageHolder> {

    private static final String LOG_TAG = "MessageListAdapter";

    private List<ChatMessage> msgDtoList = null;

    public MessageListAdapter() {
        this.msgDtoList = new ArrayList<ChatMessage>();
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        ChatMessage msgDto = this.msgDtoList.get(position);
        String orgStr = msgDto.getMsgContent();

        String[] dispStr = orgStr.split("\\:");

        // Received message.
        if(ChatMessage.MSG_TYPE_CLIENT == msgDto.getMsgType()) {
            holder.leftDisplayName.setText(dispStr[0]);
            holder.leftMsgTextView.setText(dispStr[1]);
            holder.leftMsgTextView.setTextColor(Color.BLUE);
            holder.leftDispView.setVisibility(View.GONE);
            if (null != msgDto.getThumbImage()) {
                holder.leftDispView.setVisibility(View.VISIBLE);
                holder.leftDispView.setImageBitmap(msgDto.getThumbImage());
            }

            holder.rightImageView.setVisibility(View.GONE);
            holder.rightDispView.setVisibility(View.GONE);
            holder.rightMsgTextView.setVisibility(View.GONE);
            holder.rightDisplayName.setVisibility(View.GONE);
        }
        // Sent message.
        else if(ChatMessage.MSG_TYPE_AGENT == msgDto.getMsgType()) {

            holder.rightDisplayName.setText(dispStr[0]);
            holder.rightMsgTextView.setText(dispStr[1]);
            holder.rightMsgTextView.setTextColor(Color.WHITE);
            holder.rightDispView.setVisibility(View.GONE);
            if (null != msgDto.getThumbImage()) {
                holder.rightDispView.setVisibility(View.VISIBLE);
                holder.rightDispView.setImageBitmap(msgDto.getThumbImage());
            }

            holder.leftDispView.setVisibility(View.GONE);
            holder.leftImageView.setVisibility(View.GONE);
            holder.leftMsgTextView.setVisibility(View.GONE);
            holder.leftDisplayName.setVisibility(View.GONE);
        }
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.layout_message_view, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if(msgDtoList == null) {
            msgDtoList = new ArrayList<ChatMessage>();
        }
        Timber.d("Message count: " + msgDtoList.size());
        return msgDtoList.size();
    }

    public void appendTranscript(ChatV2 message, String str){
        int displayType;
        if (message.getFrom().getType().toString().equals("Agent")) {
            displayType = ChatMessage.MSG_TYPE_AGENT;
        } else {
            displayType = ChatMessage.MSG_TYPE_CLIENT;
        }

        ChatMessage chatMessage = new ChatMessage(message.getFrom().getNickname() + str,
                displayType, null);
        msgDtoList.add(chatMessage);
        notifyItemInserted(getItemCount() - 1);
    }

    public void appendTranscriptThumbnail(Bitmap thumb, String Str, Boolean isFileTransfer) {
        ChatMessage chatMessage;
        Str = new File(Str).getName();

        if( isFileTransfer) {
            chatMessage = new ChatMessage("File Uploaded :" + Str,
                    ChatMessage.MSG_TYPE_CLIENT, thumb);
        } else {
            chatMessage = new ChatMessage("File Downloaded :" + Str,
                    ChatMessage.MSG_TYPE_AGENT, thumb);
        }

        msgDtoList.add(chatMessage);
        notifyItemInserted(getItemCount() - 1);

    }
}
