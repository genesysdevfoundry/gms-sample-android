package com.genesys.gms.mobile.gms_app;

/*
 * Copyright (C) 2015 Genesys
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Date;

import timber.log.Timber;

/**
 * Created by stau on 12/4/2014.
 */
public class AboutDialogFragment extends DialogFragment {
    private String buildChangeSet = "";
    private String buildTime = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceBundle) {
            getBuildInformationFromPropertiesFile();
            View view = inflater.inflate(R.layout.about_dialog, container, false);
            TextView versionNumber = (TextView) view.findViewById(R.id.txt_value_version);
            versionNumber.setText(buildChangeSet);
            TextView buildDate = (TextView) view.findViewById(R.id.txt_value_date);
            buildDate.setText(buildTime);

            getDialog().setTitle("About " + getResources().getString(R.string.title_activity_genesys_sample));
            getDialog().setCanceledOnTouchOutside(true);
            return view;
    }

    public void getBuildInformationFromPropertiesFile() {
        try {
            buildChangeSet = new String(BuildConfig.CHANGESET);
            buildTime = new Date(BuildConfig.TIMESTAMP).toString();
            Timber.d("Build informations | Revision: " + buildChangeSet + ", Build Date: " + buildTime);
        } catch (Exception e) {
            Timber.d("Exception: " + e);
            e.printStackTrace();
        }
    }
}

