package com.genesys.gms.mobile.gms_app.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.genesys.gms.mobile.gms_app.MainActivity;
import com.genesys.gms.mobile.gms_app.R;
import com.genesys.gms.mobile.utils.SettingsManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by pbalakri on 24-01-2018.
 */

public class FcmMessagingService extends FirebaseMessagingService {

    private static final String LOG_TAG = "FcmMessagingService";

    public FcmMessagingService() { }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Timber.d( "remoteMessage: " + remoteMessage);
        Timber.d( "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Timber.d(  "Message data payload: " + remoteMessage.getData());
            if(!isDuplicateFcmMsg(remoteMessage.getData().get("message"))) {
                SettingsManager settingsManager = SettingsManager.getInstance();
                settingsManager.handleMessage(remoteMessage.getData().get("message"));
            }
        }

    }

    private Map<String, String> parseKeyandString(JSONObject jsonObject) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            Iterator key = jsonObject.keys();
            while (key.hasNext()) {
                String k = key.next().toString();
                if (k.equalsIgnoreCase("_id") || k.equalsIgnoreCase("_action")) {
                    result.put(k, jsonObject.getString(k));
                }
            }
        } catch (JSONException e) {
            result = null;
            Timber.d("Exception: " + e );
        }
        return result;
    }

    private boolean isDuplicateFcmMsg(String jsonMsg) {
        Map<String, String> entry = null;

        try {
            JSONObject jsonObject = new JSONObject(jsonMsg);
            entry = parseKeyandString(jsonObject);
        } catch(JSONException e) {

        }

        Timber.d( "Identifying Duplicate messages" );
        Map<String, String> prevEntry = loadFcmMsgMap();

        if (entry == null || prevEntry == null) {
            saveFcmMsgMap(entry);
            return false;
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        long oldTime = prefs.getLong("msgTimestamp",0);
        long currTime = System.currentTimeMillis();
        if ((currTime - oldTime ) < 10000/*10 secs*/) {

            for (String ch : entry.keySet()) {
                if (!entry.get(ch).equalsIgnoreCase(prevEntry.get(ch))) {
                    saveFcmMsgMap(entry);
                    Timber.d( "Not a Duplicate message" );
                    return false;
                }
            }
            Timber.d( "Duplicate message" );
            saveFcmMsgMap(entry);
            return true;
        }

        return false;
    }

    private void saveFcmMsgMap(Map<String, String> entry) {
        if (entry != null) {
            //convert to string using gson
            String hashMapString = new Gson().toJson(entry);

            //save in shared prefs
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("fcmMessage", hashMapString);
            editor.putLong("msgTimestamp", System.currentTimeMillis());
            editor.apply();
        }
    }

    private Map<String, String> loadFcmMsgMap(){
        //get from shared prefs
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String storedHashMapString = prefs.getString("fcmMessage", "Error");
        if (storedHashMapString.equalsIgnoreCase("Error")) {
            return null;
        }

        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        return new Gson().fromJson(storedHashMapString, type);
    }

    /*
    @Override
    public void handleIntent(Intent intent) {
        try
        {
            if (intent.getExtras() != null) {
                RemoteMessage.Builder remoteMsg = new RemoteMessage.Builder("FcmMessagingService");
                for (String key : intent.getExtras().keySet()) {
                    Log.d(LOG_TAG, "handleIntent: " + intent.getExtras().get(key).toString());
                    remoteMsg.addData(key, intent.getExtras().get(key).toString());
                }

               bus.postSticky(new FcmReceiveEvent(remoteMsg.build(), false));

            } else {
                super.handleIntent(intent);
            }
        } catch (Exception e) {
            super.handleIntent(intent);
        }
    }
*/
    /* This method is only generating push notification */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Firebase Push Notification")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
