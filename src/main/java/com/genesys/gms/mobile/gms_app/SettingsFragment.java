package com.genesys.gms.mobile.gms_app;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import timber.log.Timber;

/**
 * Created by pbalakri on 06-12-2017.
 */

public class SettingsFragment extends Fragment {

    private EditText hostname;
    private EditText port;
    private Switch protocol;
    private EditText apiKey;
    private EditText app;
    private EditText apiVersion;
    private EditText serviceName;
    private EditText serviceNameV2;
    private Switch requestCode;
    private EditText phoneNo;
    private Switch pushNoti;
    private EditText fcmSenderId;
    private EditText simpleService;
    private String cometUrl;
    private View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_settings, container, false);
        initSettingsData(rootView);
        myView = rootView;
        return rootView;
    }

    public static SettingsFragment create(int position) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt("preferences", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveSettingsData();
    }

    private void initSettingsData(View view) {
        Timber.d("Initialize data");
        hostname = view.findViewById(R.id.hostname_edit);
        port = view.findViewById(R.id.port_edit);
        protocol = view.findViewById(R.id.secure_protocol_switch);
        apiKey = view.findViewById(R.id.apikey_edit);
        app = view.findViewById(R.id.app_edit);
        apiVersion = view.findViewById(R.id.apiversion_edit);
        serviceName = view.findViewById(R.id.servicename_edit);
        serviceNameV2 = view.findViewById(R.id.chatv2servicename_edit);
        requestCode = view.findViewById(R.id.request_code);
        phoneNo = view.findViewById(R.id.myphoneno_edit);
        pushNoti = view.findViewById(R.id.push_notifications);
        pushNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                displayFcmToken(view);
            }
        });

        fcmSenderId = view.findViewById(R.id.fcmsenderid_edit);
        simpleService = view.findViewById(R.id.simpleservice_edit);

        cometUrl = protocol.isChecked() ? "https" : "http" + "://"
                + hostname.getText().toString()
                + ":" + port.getText().toString()
                + "/genesys/cometd";

        loadSettingsData();
        saveSettingsData();

        // Update FCM Token
        //displayFcmToken(view);
    }

    private void displayFcmToken(View view) {
        TextView tv = view.findViewById(R.id.fcmtoken_edit);
        if(pushNoti.isChecked()) {
            Toast.makeText(getActivity(), "FCM Registered!", Toast.LENGTH_SHORT).show();
            tv.setText(FirebaseInstanceId.getInstance().getToken());
        } else {
            Toast.makeText(getActivity(), "FCM Unregistered!", Toast.LENGTH_SHORT).show();
            tv.setText("");
        }
    }

    private void loadSettingsData() {
        Timber.d("Load default data");
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        hostname.setText(mSettings.getString("hostname", "localhost"));
        port.setText(Integer.toString(mSettings.getInt("port", 8080)));
        protocol.setChecked(mSettings.getBoolean("protocol", false));
        apiKey.setText(mSettings.getString("apikey", "<empty>"));
        app.setText(mSettings.getString("app", "genesys"));
        apiVersion.setText(mSettings.getString("apiversion", "1"));
        serviceName.setText(mSettings.getString("servicename", "samples"));
        serviceNameV2.setText(mSettings.getString("servicenamev2", "customer-support"));
        requestCode.setChecked(mSettings.getBoolean("requestcode", false));
        phoneNo.setText(mSettings.getString("phoneno", ""));
        pushNoti.setChecked(mSettings.getBoolean("pushnotification", true));
        fcmSenderId.setText(mSettings.getString("fcmsenderid", ""));
        simpleService.setText(mSettings.getString("simpleservice", "<empty>"));
    }

    public void saveSettingsData(){
        Timber.d("Save user data");
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("hostname", hostname.getText().toString());
        editor.putInt("port", Integer.parseInt(port.getText().toString()));
        editor.putString("apikey", apiKey.getText().toString());
        editor.putString("app", app.getText().toString());
        editor.putString("apiversion", apiVersion.getText().toString());
        editor.putString("servicename", serviceName.getText().toString());
        editor.putString("servicenamev2", serviceNameV2.getText().toString());
        editor.putString("phoneno", phoneNo.getText().toString());
        editor.putString("fcmsenderid", fcmSenderId.getText().toString());
        editor.putString("simpleservice", simpleService.getText().toString());
        editor.putBoolean("protocol", protocol.isChecked());
        editor.putBoolean("requestcode", requestCode.isChecked());
        editor.putBoolean("pushnotification", pushNoti.isChecked());
        editor.putString("fcmtoken", FirebaseInstanceId.getInstance().getToken());
        editor.apply();
    }
}
