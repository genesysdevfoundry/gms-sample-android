package com.genesys.gms.mobile.gms_app;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.genesys.gms.mobile.utils.ScenarioType;
import com.genesys.gms.mobile.utils.Settings;
import com.genesys.gms.mobile.utils.SettingsManager;
import com.genesys.gms.mobile.utils.TimeHelper;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by pbalakri on 05-12-2017.
 */

public class ConnectFragment extends Fragment {

    private Spinner listView;
    private ArrayAdapter<String> listAdapter ;
    private static int spinnerIndex = -1;

    private Spinner listAvailableTimeView;
    private ArrayAdapter<String> listAvailableTimeAdapter;
    private static int availableTimeSpinnerIndex = -1;

    private EditText firstName;
    private EditText lastName;
    private EditText userName;
    private EditText displayName;
    private EditText eMail;
    private EditText subject;
    private Button desiredTime;
    private View myView;

    private String desiredTimeISO;
    private String desiredTimeforDisplay;

    private String[] availableTimeList;
    private String[] availableTimeISOList;

    private final Calendar calendar = Calendar.getInstance();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_connect, container, false);
        myView = rootView;
        initializeData(rootView);
        return rootView;
    }

    public static ConnectFragment create(int position) {
        ConnectFragment fragment = new ConnectFragment();
        Bundle args = new Bundle();
        args.putInt("preferences", position);
        fragment.setArguments(args);
        return fragment;
    }

    private void initSpinner(View view){
        Timber.d("Initialize Spinner");
        listView = (Spinner)view.findViewById(R.id.scenario_edit);
        String[] scenariosList = getResources().getStringArray(R.array.scenarios);
        listAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, scenariosList);
        listAdapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        listView.setAdapter(listAdapter);
        listView.setSelection(spinnerIndex);

        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // On selecting a spinner item
                if ((spinnerIndex == -1) || (spinnerIndex != i)) {
                    spinnerIndex = i;
                    setScenarioMenu();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(adapterView.getContext(), "Nothing Selected: ", Toast.LENGTH_LONG).show();
            }

        });
    }

    @Override
    public void onPause() {
        super.onPause();
        saveConnectData();
    }

    private void initializeData(View view) {
        Timber.d("Initialize data");
        firstName = view.findViewById(R.id.firstname_edit);
        lastName = view.findViewById(R.id.lastname_edit);
        userName = view.findViewById(R.id.username_edit);
        displayName = view.findViewById(R.id.displayname_edit);
        eMail = view.findViewById(R.id.email_edit);
        subject = view.findViewById(R.id.subject_edit);
        desiredTime = view.findViewById(R.id.time_edit);

        desiredTime.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                final Calendar currentDate = Calendar.getInstance();
                Calendar date = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_DeviceDefault_Light_Dialog_Alert ,
                        new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                        date.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(getActivity(),
                                android.R.style.Theme_DeviceDefault_Light_Dialog_Alert,
                                new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                date.set(Calendar.MINUTE, minute);

                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                                desiredTimeISO = df.format(date.getTimeInMillis())+"Z";
                                desiredTimeforDisplay = new SimpleDateFormat("dd-MMM-yyyy h:mm a").format(date.getTime());
                                desiredTime.setText(desiredTimeforDisplay);

                                // Get Available Time slots based on the date and time selected.
                                getAvailableTimeSlots(desiredTimeISO);

                            }
                        },currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();

                    }
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));
                datePickerDialog.getDatePicker().setMinDate(currentDate.getTimeInMillis());
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.show();
            }
        });

        loadConnectData();
        initSpinner(view);
        String[] displaySpinnerStr = {"Select desired time above"};
        initTimeSlotsSpinner(view, displaySpinnerStr);
        setScenarioMenu();
    }

    private void setScenarioMenu() {
        if((spinnerIndex != -1) && (ScenarioType.fromString(ScenarioType.scenarios()[spinnerIndex])
                == ScenarioType.VOICE_SCHEDULED_USERTERM)) {
            listAvailableTimeView.setEnabled(true);
            desiredTime.setEnabled(true);
        } else {
            listAvailableTimeView.setEnabled(false);
            desiredTime.setEnabled(false);
        }
    }

    private void getAvailableTimeSlots(String desiredTime) {
        Timber.d("Get available time slots for: " + desiredTime);
        SettingsManager settingsMgr = SettingsManager.getInstance();
        Settings settings = settingsMgr.getDatas();
        Toast.makeText(getActivity(), "Getting Available time slots...", Toast.LENGTH_SHORT).show();
        settingsMgr.queryAvailableTimeSlots(settings.getServiceName(), desiredTime);
    }

    private void initTimeSlotsSpinner(View view, String[] timeList){
        Timber.d("Available time slots: " + Arrays.toString(timeList));
        listAvailableTimeView = (Spinner)view.findViewById(R.id.availabletime_edit);
        listAvailableTimeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeList);
        listAvailableTimeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        listAvailableTimeView.setAdapter(listAvailableTimeAdapter);
        listAvailableTimeView.setSelection(availableTimeSpinnerIndex);

        listAvailableTimeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // On selecting a spinner item
                if ((availableTimeSpinnerIndex == -1) || (availableTimeSpinnerIndex != i)) {
                    availableTimeSpinnerIndex = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(adapterView.getContext(), "Nothing Selected: ", Toast.LENGTH_LONG).show();
            }

        });
    }


    public void availableTimeslots(Map<String, String> availability) {
        List<Pair<String, String>> newEntries = new ArrayList<Pair<String, String>>();

        String timeStr;
        DateTime timeSlot;
        Integer availableSlot;
        String friendlyTime;
        String strTimeSlot;

        // TODO: Opportunity to use RxJava/functional approach here
        for (Map.Entry<String, String> entry : availability.entrySet()) {
            timeStr = entry.getKey();
            timeSlot = TimeHelper.parseISO8601DateTime(timeStr);
            availableSlot = Integer.parseInt(entry.getValue());

            if (availableSlot != null && availableSlot > 0) {
                friendlyTime = TimeHelper.toFriendlyString(timeSlot);
                strTimeSlot = TimeHelper.serializeDefaultTime(timeSlot);
                newEntries.add(new Pair<String, String>(
                        friendlyTime, strTimeSlot
                ));
            }
        }
        if (newEntries.size() == 0) {
            Toast.makeText(getActivity(), "No time slots available", Toast.LENGTH_LONG).show();
            String[] displaySpinnerStr = {"No time slots available"};
            initTimeSlotsSpinner(myView, displaySpinnerStr);
        } else {
            String desiredTime;
            int closestTimeIndex;
            int firstIndex;
            int lastIndex;
            int availableEntries;
            final int AVAILABLE_OPTIONS = 10;

            Collections.sort(newEntries, new Comparator<Pair<String, String>>() {
                @Override
                public int compare(Pair<String, String> pair1, Pair<String, String> pair2) {
                    return pair1.second.compareTo(pair2.second);
                }
            });

            // binary search
            closestTimeIndex = findClosestTime(newEntries, desiredTimeISO);
            firstIndex = Math.max(closestTimeIndex - AVAILABLE_OPTIONS / 2, 0);
            lastIndex = Math.min(closestTimeIndex + AVAILABLE_OPTIONS / 2, newEntries.size() - 1);
            availableEntries = lastIndex - firstIndex + 1;

            availableTimeList = new String[availableEntries];
            availableTimeISOList = new String[availableEntries];
            int i = 0;
            for (int j = firstIndex; j <= lastIndex; ++j) {

                availableTimeList[i] = newEntries.get(j).first;
                availableTimeISOList[i] = newEntries.get(j).second;
                ++i;
            }
            initTimeSlotsSpinner(myView,availableTimeList);
        }
        listAvailableTimeView.setEnabled(true);
    }

    /** Binary search implementation which finds closest match */
    private int findClosestTime(List<Pair<String, String>> availableTimes, String desiredTime) {
        Timber.d("Find closest time for: " + desiredTime);
        int lo = 0;
        int hi;
        int mid = -1;
        int result;
        if (availableTimes == null || availableTimes.isEmpty() || desiredTime == null) {
            return -1;
        }
        hi = availableTimes.size() - 1;
        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            result = desiredTime.compareTo(availableTimes.get(mid).second);
            if (result < 0) {
                // less than mid
                hi = mid - 1;
            } else if (result > 0) {
                // greater than mid
                lo = mid + 1;
            } else {
                // equal, use this
                Timber.d("Closest time for: " + desiredTime + " is: " + mid);
                return mid;
            }
        }
        Timber.d("Closest time for: " + desiredTime + " is: " + mid);
        return mid;
    }

    private void loadConnectData() {
        Timber.d("Load default data");
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        firstName.setText(mSettings.getString("firstname", "John"));
        lastName.setText(mSettings.getString("lastname", "Doe"));
        userName.setText(mSettings.getString("username", "999"));
        displayName.setText(mSettings.getString("displayname", "John"));
        eMail.setText(mSettings.getString("email", "john.doe@test.com"));
        subject.setText(mSettings.getString("subject", "Genesys Mobile Services Demo"));
        spinnerIndex = mSettings.getInt("index", -1);
        desiredTime.setText(mSettings.getString("desiredtimedisplay", "Tap to select"));
    }

     public void saveConnectData(){
        Timber.d("Save user data");
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("firstname", firstName.getText().toString());
        editor.putString("lastname", lastName.getText().toString());
        editor.putString("username", userName.getText().toString());
        editor.putString("displayname", displayName.getText().toString());
        editor.putString("email", eMail.getText().toString());
        editor.putString("subject", subject.getText().toString());
        editor.putInt("index", spinnerIndex);
        if(availableTimeISOList != null)
            editor.putString("desiredtime", availableTimeISOList[availableTimeSpinnerIndex]);

        editor.putString("desiredtimedisplay", desiredTimeforDisplay);
        editor.apply();
    }
}
