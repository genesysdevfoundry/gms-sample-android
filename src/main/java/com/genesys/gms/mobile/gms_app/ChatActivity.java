package com.genesys.gms.mobile.gms_app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.genesys.gms.mobile.client.ChatClient;
import com.genesys.gms.mobile.client.ChatHandler;
import com.genesys.gms.mobile.data.api.pojo.ChatV2;
import com.genesys.gms.mobile.gms_app.adapter.MessageListAdapter;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * Created by pbalakri on 18-01-2018.
 */

public class ChatActivity extends AppCompatActivity implements ChatHandler {

    private static final  String LOG_TAG = "ChatActivity";

    private ChatClient chatClient;

    private RecyclerView mMsgRecyclerView;
    private MessageListAdapter mMsgAdapter;
    private ScheduledFuture<?> scheduledStopTypingMessage;
    private ScheduledFuture<?> scheduledWipeInformationalMessage;
    private static final ScheduledExecutorService timer = Executors.newScheduledThreadPool(2);
    private boolean userTyping;

    private Button sendButton;
    private EditText sendText;
    private TextView typingPreview;
    private TextWatcher textWatcher;
    private Boolean chatFinished;
    private static boolean isFileTransfer = false;
    private static final int SELECT_FILE = 10;
    private Uri uri;
    private String currentFileId;
    private final int READ_FILE_ID = 1;
    private final int WRITE_FILE_ID = 2;
    private static final int NID_CHAT_ACTIVITY = 10;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg);
        MySSLSocketFactory sslSocketFactory = MySSLSocketFactory.getInstance();
        chatClient = new ChatClient(this, this, sslSocketFactory.getSslContextFactory());

        mMsgRecyclerView = findViewById(R.id.msg_recycler_view);
        mMsgAdapter = new MessageListAdapter();
        mMsgRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMsgRecyclerView.setAdapter(mMsgAdapter);
        sendButton =  findViewById(R.id.sendbuton);
        sendText = findViewById(R.id.sendtxt);
        typingPreview = findViewById(R.id.typing_preview);
        typingPreview.setTextColor(Color.GREEN);
        chatFinished = false;

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatClient.sendMessage(sendText.getText().toString());
                sendText.setText("");
                updateSendButtonState();
            }
        });
        updateSendButtonState();
        initTextWatcher();
        chatClient.connect();
    }




    @Override
    public void onBackPressed() {
       if(chatFinished) super.onBackPressed();
    }

    @Override
    protected void onResume() {
        //NotificationManagerCompat.from(this).cancel(NID_CHAT_ACTIVITY);
        if (chatFinished) {
            finishChat();
        }

        final NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        //NotificationManagerCompat.from(this).cancel(NID_CHAT_ACTIVITY);
        chatClient.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (userTyping) {
            setUserTyping(false);
            if (scheduledStopTypingMessage != null && !scheduledStopTypingMessage.isDone()) {
                scheduledStopTypingMessage.cancel(false);
                scheduledStopTypingMessage = null;
            }
        }
        //if (!chatFinished) generateNotification();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_exit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.exit_chat) {
            chatFinished = true;
            chatClient.disconnect();
            //NotificationManagerCompat.from(this).cancel(NID_CHAT_ACTIVITY);
            this.finish();
        }
        return true;
    }

    private void generateNotification() {
        Intent intentReturnToChat = new Intent(this, ChatActivity.class);
        intentReturnToChat.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentReturn = PendingIntent.getActivity(
                this, 0, intentReturnToChat, PendingIntent.FLAG_ONE_SHOT);

        NotificationManagerCompat.from(this).notify(
                NID_CHAT_ACTIVITY,
                new Notification.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setOngoing(true)
                        .setContentTitle("GMS")
                        .setContentIntent(pendingIntentReturn)
                        .setContentText(getResources().getString(R.string.chat_in_progress))
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_LIGHTS)
                        .build()
        );
    }

    private void finishChat() {
        Timber.d("Chat session ended");
        chatFinished = true;
        updateSendButtonState();
        if (textWatcher != null) {
            sendText.removeTextChangedListener(textWatcher);
            textWatcher = null;
        }
        sendText.setText("");
        sendText.setEnabled(false);
        showPermanentInfo("Chat finished");
        finishAffinity();
        //NotificationManagerCompat.from(this).cancel(NID_CHAT_ACTIVITY);
    }


    private final Runnable updateUserTyping = new Runnable() {
        @Override
        public void run() {
            // Stop Typing preview
            if (userTyping) {
                setUserTyping(false);

                if (scheduledStopTypingMessage != null) {
                    if (!scheduledStopTypingMessage.isDone()) {
                        scheduledStopTypingMessage.cancel(false);
                    }
                    scheduledStopTypingMessage = null;
                }
                userTyping = false;
            }
        }
    };

    private void updateSendButtonState() {
        sendButton.setEnabled(!chatFinished && sendText.length() > 0);
    }

    private void initTextWatcher () {
       textWatcher = new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
               if (!userTyping && count > before) {
                   userTyping = true;

               } else if (scheduledStopTypingMessage != null && !scheduledStopTypingMessage.isDone()) {
                   scheduledStopTypingMessage.cancel(false);
                   scheduledStopTypingMessage = null;
               }
           }

           @Override
           public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
               updateSendButtonState();
           }

           @Override
           public void afterTextChanged(Editable editable) {
               // Start Typing preview
               if (userTyping) setUserTyping(true);

               if (scheduledStopTypingMessage == null || scheduledStopTypingMessage.isDone()) {
                   scheduledStopTypingMessage = timer.schedule(updateUserTyping, 5, TimeUnit.SECONDS);
               }
           }
       };
       sendText.addTextChangedListener(textWatcher);
    }

    public void setUserTyping (final boolean isUserTyping) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(isUserTyping) chatClient.startTyping();
                else chatClient.stopTyping();
            }
        });
    }

    public void setTranscriptInfo (final String str) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                typingPreview.setText(str);
            }
        });
    }

    private void showPermanentInfo(String text) {
        showInfoImpl(text, true);
    }

    private void showInfoImpl(String text, boolean permanent) {
        if (scheduledWipeInformationalMessage != null) {
            scheduledWipeInformationalMessage.cancel(false);
        }

        setInformationalMessageImpl(text);

        if (!permanent) {
            scheduledWipeInformationalMessage = timer.schedule(wipeInformationalMessage, 10, TimeUnit.SECONDS);
        }
    }

    private void setInformationalMessageImpl(final String text) {
        typingPreview.setText(text);
    }

    private final Runnable wipeInformationalMessage = new Runnable() {
        @Override
        public void run() {
            new Executor(){
                @Override
                public void execute(@NonNull Runnable command) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setInformationalMessageImpl("");
                        }
                    });
                }
            };
        }
    };

    @Override
    public void onMessage(final ChatV2 message) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                switch (message.getType()){
                    case MESSAGE:
                        if(message.getFrom().getType().toString().equals("Agent")) setTranscriptInfo("");
                        mMsgAdapter.appendTranscript(message, " says: " + message.getText());
                        break;

                    case PARTICIPANT_JOINED:
                        mMsgAdapter.appendTranscript(message, ": joined the conversation");
                        Timber.d("Participant joined");
                        break;

                    case PARTICIPANT_LEFT:
                        chatFinished = true;
                        updateSendButtonState();
                        mMsgAdapter.appendTranscript(message, ": left the conversation");
                        Timber.d("Participant left");
                        break;

                    case TYPING_STARTED:
                        setTranscriptInfo(message.getFrom().getNickname() + " typing ..." );
                        break;

                    case TYPING_STOPPED:
                        setTranscriptInfo("");
                        break;

                    case FILE_UPLOADED:
                        downloadFile(message.getText());
                        Timber.d("File uploaded");
                        break;

                    default:
                        break;
                }
                mMsgRecyclerView.scrollToPosition(mMsgAdapter.getItemCount() - 1);
            }
        });
    }

    @Override
    public void onFileDownloaded(String s) {
        Timber.d("OnFileDownloaded : " + s);
        Bitmap thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(s), 250, 250);
        mMsgAdapter.appendTranscriptThumbnail(thumbImage, s, isFileTransfer);
        isFileTransfer = false;
    }

    @Override
    public void onFileUploaded(Uri uri) {
        Timber.d("onFileUploaded : " + uri.toString());
    }

    @Override
    public void onError(Exception e) {
        Timber.d("onError : " + e.getMessage());
    }

    public void downloadFile(String fileId) {
        Timber.d("Download file: " + fileId);
        currentFileId = fileId;
        if(android.os.Build.VERSION.SDK_INT  >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            } else {
                chatClient.downloadFile(fileId);
            }
        } else {
            chatClient.downloadFile(fileId);
        }

    }

    public void transferFile(View view) {
        showFileChooser();
    }

    private void showFileChooser() {
        Intent chooser = new Intent(Intent.ACTION_GET_CONTENT);
        Uri uri = Uri.parse(Environment.getDownloadCacheDirectory().getPath().toString());
        chooser.addCategory(Intent.CATEGORY_OPENABLE);
        chooser.setDataAndType(uri, "*/*");
        try {
            startActivityForResult(chooser, SELECT_FILE);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            Toast.makeText(this, getResources().getString(R.string.install_file_manager),
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            uri = data.getData();
            if(android.os.Build.VERSION.SDK_INT  >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            READ_FILE_ID);
                } else {
                    chatClient.transferFile(uri);
                }
            } else {
                chatClient.transferFile(uri);
            }
            isFileTransfer = true;
            Toast.makeText(this, this.getContentResolver().getType(uri), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_FILE_ID: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chatClient.transferFile(uri);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_read), Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case WRITE_FILE_ID: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chatClient.downloadFile(currentFileId);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_write), Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

}
