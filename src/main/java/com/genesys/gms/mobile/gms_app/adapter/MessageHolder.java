package com.genesys.gms.mobile.gms_app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.genesys.gms.mobile.gms_app.R;

/**
 * Created by pbalakri on 19-01-2018.
 */

public class MessageHolder extends RecyclerView.ViewHolder {


    ImageView leftImageView;
    TextView leftDisplayName;
    TextView leftMsgTextView;
    ImageView leftDispView;


    ImageView rightImageView;
    TextView rightDisplayName;
    TextView rightMsgTextView;
    ImageView rightDispView;


    public MessageHolder(View itemView) {
        super(itemView);

        if(itemView!=null) {
            leftDisplayName = (TextView) itemView.findViewById(R.id.left_display_name);
            leftMsgTextView = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
            leftImageView = (ImageView) itemView.findViewById(R.id.left_iconImage);
            leftDispView = (ImageView) itemView.findViewById(R.id.left_imageView);

            rightDisplayName = (TextView) itemView.findViewById(R.id.right_display_name);
            rightMsgTextView = (TextView) itemView.findViewById(R.id.chat_right_msg_text_view);
            rightImageView = (ImageView) itemView.findViewById(R.id.right_iconImage);
            rightDispView = (ImageView) itemView.findViewById(R.id.right_imageView);

        }
    }
}