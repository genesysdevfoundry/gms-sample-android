package com.genesys.gms.mobile.gms_app.push;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import timber.log.Timber;

/**
 * Created by pbalakri on 24-01-2018.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {
    private static final String LOG_TAG = "FcmInstanceIdService";


    @Override
    public void onTokenRefresh() {
         /* Getting registration token */
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        /* Displaying token on logcat */
        Timber.d("Refreshed token: " + refreshedToken);        

        /* Post using the Event Bus */
    }

    // TODO: Move into utility class
    private int getAppVersion() {
        try {
            PackageInfo packageInfo = this.getApplicationContext().getPackageManager()
                    .getPackageInfo(this.getApplicationContext().getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            //Timber.e(e, "Failed to obtain application version code.");
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

}
