package com.genesys.gms.mobile.gms_app;

import android.util.Log;

import org.eclipse.jetty.util.ssl.SslContextFactory;

import java.security.KeyStore;
import java.util.Arrays;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class MySSLSocketFactory  {

    private static MySSLSocketFactory mySSLSocketFactory = null;
    private static X509TrustManager trustManager = null;
    private static SSLSocketFactory sslSocketFactory = null;
    private static SslContextFactory contextFactory = null;

    public MySSLSocketFactory() {
        createTrustManager();
        createSSLSocketFactory();
        createSslContextFactory();
    }

    public static MySSLSocketFactory getInstance() {
        if(mySSLSocketFactory == null) {
            mySSLSocketFactory = new MySSLSocketFactory();
        }
        return mySSLSocketFactory;
    }

    // For retrofit
    private void createTrustManager() {
        try {
            if(trustManager == null) {
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init((KeyStore) null);
                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
                }
                trustManager = (X509TrustManager) trustManagers[0];
            }
        } catch (Exception ex){
            Log.e("MySSLSocketFactory", "Exception :" + ex);
        }
    }

    // For retrofit
    private void createSSLSocketFactory() {
        try {
            if(sslSocketFactory == null) {
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, new TrustManager[]{trustManager}, null);
                sslSocketFactory = sslContext.getSocketFactory();
            }
        } catch (Exception ex){
            Log.e("MySSLSocketFactory", "Exception :" + ex);
        }
    }

    //For chatClient
    private void createSslContextFactory() {
        contextFactory = new SslContextFactory(true);// or  SslContextFactory();
        //Include necessary SSL Settings
        //contextFactory.setIncludeProtocols("TLSv1.2");
        //contextFactory.setExcludeProtocols(excludeProtocols);
        //contextFactory.setExcludeCipherSuites(excludeCiphers);
        //contextFactory.setIncludeCipherSuites(includeCiphers);
        //contextFactory.setKeyStore(keyStore);
        //contextFactory.setNeedClientAuth(needClientAuth);
        //contextFactory.setProtocol(SslAlgorithm);
    }

    public SSLSocketFactory getSSLSocketFactory() {
        return sslSocketFactory;
    }

    public X509TrustManager getTrustManager() {
        return trustManager;
    }

    public SslContextFactory getSslContextFactory() {
        return contextFactory;
    }
}


