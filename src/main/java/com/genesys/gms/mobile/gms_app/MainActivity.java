package com.genesys.gms.mobile.gms_app;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.genesys.gms.mobile.client.SettingsHandler;
import com.genesys.gms.mobile.data.api.pojo.DialogResponse;
import com.genesys.gms.mobile.gms_app.log.FileLoggingTree;
import com.genesys.gms.mobile.gms_app.log.LogActivity;
import com.genesys.gms.mobile.utils.CallbackUtils;
import com.genesys.gms.mobile.utils.ScenarioType;
import com.genesys.gms.mobile.utils.Settings;
import com.genesys.gms.mobile.utils.SettingsManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;


public class MainActivity extends AppCompatActivity implements SettingsHandler{

    private CallbackUtils callbackApi;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private SettingsManager settingsMgr;
    private TabLayout tabLayout;

    private ConnectFragment connectF;
    private SettingsFragment settingsF;
    private QueueFragment queueF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Timber.d("Current Tab: " + position);
                saveSettingsData();
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        callbackApi = new CallbackUtils(this);
        MySSLSocketFactory sslSocketFactory = MySSLSocketFactory.getInstance();
        settingsMgr = SettingsManager.createInstance(this, this,
                sslSocketFactory.getTrustManager(), sslSocketFactory.getSSLSocketFactory());


        if (BuildConfig.DEBUG) {
            // Timber is a logging library which automatically handles tagging
            // The LogbackFacadeTree will log messages to a file (for in-app viewing)
            Timber.plant(new FileLoggingTree(new Timber.DebugTree(), this));
        } else {
            // TODO: Figure out release logging
            Timber.plant(new FileLoggingTree(new Timber.HollowTree(), this));
        }


        Timber.d("Main Activity created");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Timber.d("Main Activity onNewIntent");
        Intent intentReturnToChat = new Intent(this, ChatActivity.class);
        intentReturnToChat.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intentReturnToChat);
    }

    @Override
    public void onError(Exception e) {
        Timber.d("Unexpected exception: " + e);
        displayToastMsg("Unexpected exception");
    }

    @Override
    public void dial(DialogResponse dialog) {

        if(dialog == null) {
            Timber.d("Response is null");
        } else if ((dialog!= null) && (dialog.getAction() == null)) {
            String[] scenariosList = getResources().getStringArray(R.array.scenarios);
            if (ScenarioType.fromString(scenariosList[getScenarioIndex()]) ==
                    ScenarioType.VOICE_SCHEDULED_USERTERM) {
                displayToastMsg("Callback scheduled at selected time");
                Timber.d("Callback scheduled at selected time");
                saveServiceId(dialog.getId());
            } else {
                Timber.d("Error");
            }
        } else {
            switch (dialog.getAction()) {
                case DIAL:
                    Timber.d("dial func: " + dialog.getLabel());
                    callbackApi.makeCall(Uri.parse(dialog.getTelUrl()));
                    break;
                case MENU:
                    if (dialog.getContent() == null || dialog.getContent().size() == 0) {
                        // It's empty! No menu to show...
                        break;
                    }
                    if(dialog.getId() != null)
                        saveServiceId(dialog.getId());

                    DialogResponse.DialogGroup group = dialog.getContent().get(0);
                    String groupName = group.getGroupName();
                    final List<DialogResponse.GroupContent> groupContents = group.getGroupContent();
                    if (groupContents == null || groupContents.size() == 0) {
                        // It's empty! There are no options...
                        break;
                    }
                    String[] menuItems = new String[groupContents.size()];
                    for (int i = 0; i < groupContents.size(); i++) {
                   /* GMS-2245 */
                        menuItems[i] = groupContents.get(i).getLabel();
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(dialog.getLabel() + "\n" + groupName)
                            .setItems(menuItems, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String url = groupContents.get(which).getUserActionUrl();
                                    switch(which) {
                                        case 0:
                                        case 1:
                                            settingsMgr.startDialog(url);
                                            break;
                                        case 2:
                                            settingsMgr.cancelCallbackQuery(url);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            });
                    builder.create().show();
                    break;
                case CONFIRM:
                    displayToastMsg(dialog.getText());
                    Timber.d("Confirmation message: " + dialog.getText());
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void availableTimeslots(Map<String, String> availability) {
        connectF.availableTimeslots(availability);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connect, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
            case R.id.connect:
                Timber.d("Option selected: Connect");
                Map<String, String> params = new HashMap<>();
                saveSettingsData();
                Settings settings = settingsMgr.getDatas();
                String[] scenariosList = getResources().getStringArray(R.array.scenarios);
                String scenario = scenariosList[0];
                int index = getScenarioIndex();

                if(index != -1) {
                    scenario = scenariosList[index];
                }
                params = callbackApi.getScenarioParams(settings, ScenarioType.fromString(scenario));
                if (ScenarioType.fromString(scenario) == ScenarioType.CHAT_V2) {
                    Intent intent = new Intent(this, ChatActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Connecting...", Toast.LENGTH_SHORT).show();
                    settingsMgr.connect(settings.getServiceName(), params);
                }
                break;
            case R.id.log:
                Timber.d("Option selected: View log");
                startActivity(new Intent(this, LogActivity.class));
                break;
            case R.id.about:
                Timber.d("Option selected: About");
                DialogFragment dialog = new AboutDialogFragment();
                dialog.show(getFragmentManager(), "dialog_about");
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void saveSettingsData() {
        Timber.d("Save user data");

        connectF.saveConnectData();
        settingsF.saveSettingsData();

        Settings informations = new Settings();
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(this);

        informations.setFirstName(mSettings.getString("firstname", ""));
        informations.setLastName(mSettings.getString("lastname", ""));
        informations.setUserName(mSettings.getString("username", ""));
        informations.setDisplayName(mSettings.getString("displayname", ""));
        informations.setEmail(mSettings.getString("email", ""));
        informations.setSubject(mSettings.getString("subject", ""));
        informations.setHostname(mSettings.getString("hostname", ""));
        informations.setPort(mSettings.getInt("port", 8080));
        informations.setApiKey(mSettings.getString("apikey", ""));
        informations.setApp(mSettings.getString("app", ""));
        informations.setServiceName(mSettings.getString("servicename", ""));
        informations.setServiceNameV2(mSettings.getString("servicenamev2", ""));
        informations.setPhoneNumber(mSettings.getString("phoneno", ""));
        informations.setGcmSenderId(mSettings.getString("fcmsenderid", ""));
        informations.setSimpleServiceParam(mSettings.getString("simpleservice", ""));
        informations.setSecureProtocol(mSettings.getBoolean("protocol", false));
        informations.setRequestCode(mSettings.getBoolean("requestcode", false));
        informations.setPushNotification(mSettings.getBoolean("pushnotification", true));
        informations.setFcmToken(mSettings.getString("fcmtoken", ""));
        informations.setDesiredTime(mSettings.getString("desiredtime", ""));

        settingsMgr.saveDatas(informations);

        // Update FCM Token
        TextView tv = (TextView) findViewById(R.id.fcmtoken_edit);
        tv.setText(FirebaseInstanceId.getInstance().getToken());
    }

    private int getScenarioIndex() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        int scenarioIndex = mSettings.getInt("index", -1);
        Timber.d("Scenario Index: " + scenarioIndex);
        return scenarioIndex;
    }

    private void saveServiceId(String serviceId) {
        SharedPreferences mSettings =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("service_id", serviceId);
        editor.apply();
        Timber.d("Saved service id: " + serviceId);
    }

    private void displayToastMsg(String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
            }
        });
    }




    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch(position){
                case 0:
                    connectF = ConnectFragment.create(position + 1);
                    return connectF;
                case 1:
                    settingsF = SettingsFragment.create(position + 1);
                    return settingsF;
                case 2:
                    queueF = QueueFragment.create(position + 1);
                    return queueF;
                default:
                    break;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
