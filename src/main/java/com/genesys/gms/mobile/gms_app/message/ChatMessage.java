package com.genesys.gms.mobile.gms_app.message;

import android.graphics.Bitmap;

/**
 * Created by pbalakri on 18-01-2018.
 */

public class ChatMessage {

    public final static int MSG_TYPE_CLIENT = 0;
    public final static int MSG_TYPE_AGENT = 1;

    private String msgContent;// Message content.
    private int msgType;// Message type.
    private Bitmap thumbImage; // Thumbnail image

    public ChatMessage(String msgContent, int msgType, Bitmap thumbImage) {
        this.msgContent = msgContent;
        this.msgType = msgType;
        this.thumbImage = thumbImage;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public Bitmap getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(Bitmap thumbImage) {
        this.thumbImage = thumbImage;
    }
}



